var chance = new Chance();

var library = [];
var library_count = 1000000;

var lastFridayForMonth = function (monthMoment) {
  var lastDay = monthMoment.endOf('month').startOf('day');
  switch (lastDay.day()) {
    case 6:
      return lastDay.subtract(1, 'days');
    default:
      return lastDay.subtract(lastDay.day() + 2, 'days');
  }
}

for (var i = 0; i < library_count; i++) {
  var book = {
    title: chance.sentence({words: 3}),
    genre: chance.pickone(['Satire', 'Horror', 'Finance', 'Fiction', 'Politics']),
    publish_date: chance.date({string: true}),
    author_name: chance.name(),
    author_gender: chance.pickone(['boy', 'girl'])
  };

  var r = lastFridayForMonth(moment(book.publish_date))
  var stringDate = moment(r._d).format('MM/DD/YYYY');
  if (stringDate === book.publish_date) book.last_friday = 'true';

  library.push(book);
}

var options = {
  valueNames: [ 'title', 'genre', 'publish_date', 'author_name', 'author_gender', 'last_friday' ],
  item: `<div class="col-sm-4">
    <div class="card">
      <div class="media">
        <div class="media-left">
          <img class="media-object" src="img/book-cover.jpg" alt="Book cover">
        </div>
        <div class="media-body">
          <h4 class="title media-heading"></h4>
          <em><span class="genre"></span></em> |
          by <span class="author_name"></span> (<span class="author_gender"></span>)
          <p>Publication Date: <span class="publish_date"></span></p>
          <p class="last_friday hidden"></p>
        </div>
      </div>
    </div></div>`,
  pagination: true,
  page: 12
};

var books = new List('books', options, library);

$('#search-genre').on('keyup', function() {
  var searchString = $(this).val();
  books.search(searchString, ['genre']);
});

$('#search-gender').on('keyup', function() {
  var searchString = $(this).val();
  books.search(searchString, ['author_gender']);
});

$("#indicate-finance").on('click', function() {
  books.filter(function(item) {
    if (item.values().genre === 'Finance' && item.values().last_friday === 'true') {
       return true;
    } else {
       return false;
    }
  });
});

$("#indicate-horror").on('click', function() {
  books.filter(function(item) {
    if (item.values().genre === 'Horror' && item.values().publish_date.startsWith("10/31/")) {
       return true;
    } else {
       return false;
    }
  });
});
