# Marin's Library of Random Books
This is a code challenge for Casumo.

## Getting started

Make sure you have Node and npm.
After you pull the project, in your terminal type:

```
cd casumo-challenge
npm install
npm start
```

Once the server is running, the project will be available at `http://127.0.0.1:8080/` in your browser.

## Libraries used:
- Bootstrap for the quick prototype of the HTML, including some basic styling and JavaScript
- Chance.js - used to generate random data
- Moment.js - used to handle date manipulation
- http-server - used to start a development server ( thanks Casumo for the tip ;) )

Please note that loading the 1000000 books takes a bit of time. If you want to preview the work quickly, you can decrease the size of the library in the `script.js` file.

Looking forward to hearing from you!

Thanks,
Marin
marin.gerov@gmail.com
